/* DiceGame program
 * @author Pimonpan Pathike
 * 22/1/2018
 */
package pathike.pimonpan.lab3;
import java.util.Random;
import java.util.Scanner;

public class DiceGame {
	public static void main(String[] args) {
		Scanner inputNumber = new Scanner(System.in);
		Random rand= new Random();		 
        // Generate random integers in range 1 to 6
		int comRandom = rand.nextInt(6) + 1;
		System.out.print("Enter your guest (1-6) : " );
		int guestNumber = inputNumber.nextInt();
		if(6<guestNumber || guestNumber<1) {
			System.err.println("Incorrect number. Only 1-6 can be entered");
			System.exit(0);}
		System.out.println("You have guested number : " + guestNumber);
		System.out.println("Computer has rolled number : " + comRandom);			
		if(guestNumber == comRandom) {
			System.out.println("You win");
		}
		else if(guestNumber == comRandom-1) {
			System.out.println("You win");
		}
		else if(guestNumber == comRandom+1) {
			System.out.println("You win");
		}
		else if(guestNumber == 1 && comRandom == 6 || guestNumber == 6 && comRandom == 1) {
			System.out.println("You win");
		}
		else{
			System.out.println("Computer wins");
		}
	}	         
}
