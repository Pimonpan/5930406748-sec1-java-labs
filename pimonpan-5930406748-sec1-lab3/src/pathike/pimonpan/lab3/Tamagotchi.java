package pathike.pimonpan.lab3;

import java.util.Scanner;

public class Tamagotchi {
	public static void main(String[] args) {
		Scanner valueInput = new Scanner(System.in);
		System.out.print("Enter your Tamagotchi info : " );
		int liveHours = valueInput.nextInt();
		int feedHours = valueInput.nextInt();		
		int waterHours = valueInput.nextInt();	
		System.out.println("Your Tamagotchi will live for " + liveHours + " hours");
		System.out.println("It needs to be fed every " + feedHours + " hours");
		System.out.println("It needs to be watered every " + waterHours + " hours");
		System.out.println();
		int timesOfBoth = liveHours/lcm(feedHours, waterHours);
		int timesOfWater = liveHours/waterHours-timesOfBoth;
		int timesOfFeed = liveHours/feedHours-timesOfBoth;
				
		System.out.println("You need to water-feed : " + timesOfBoth + " times");
		System.out.println("You need to water : " + timesOfWater + " times");
		System.out.println("You need to feed " + timesOfFeed + " times");
	}
	
	static int lcm(int feedHours, int waterHours)//method for find lcm
	    {
	        int lcm;
	        lcm = (feedHours > waterHours) ? feedHours : waterHours; 
	        while(true)
	        {
	            if(lcm % feedHours == 0 && lcm % waterHours == 0){
	                return lcm;
	                }
	            ++lcm;
	        }
	        
	    }
}
