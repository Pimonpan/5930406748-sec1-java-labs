package pathike.pimonpan.lab3;

import java.util.Random;
import java.util.Scanner;

public class DiceMethodGame {
	static int humanGuest;
	static int computerScore;
	public static void main(String[] args) {
		humanGuest = acceptInput();
		computerScore = genDiceRoll();
		displayWinner(humanGuest,computerScore);
	}
	public static int acceptInput() {	
		Scanner inputNumber = new Scanner(System.in);
		System.out.print("Enter your guest (1-6) : " );
		int guestNumber = inputNumber.nextInt();
		if(6<guestNumber || guestNumber<1) {
			System.err.println("Incorrect number. Only 1-6 can be entered");
			System.exit(0);}
		return guestNumber;
	}
	public static int genDiceRoll() {
		Random rand= new Random();	
		int comRandom = rand.nextInt(6) + 1;
		return comRandom;
	}
	public static int displayWinner(int humanGuest,int computerScore) {
		System.out.println("You have guested number : " + humanGuest);
		System.out.println("Computer has rolled number : " + computerScore);
		if(humanGuest == computerScore) {
			System.out.println("You win");
		}
		else if(humanGuest == computerScore-1) {
			System.out.println("You win");
		}
		else if(humanGuest == computerScore+1) {
			System.out.println("You win");
		}
		else if(humanGuest == 1 && computerScore == 6 || humanGuest == 6 && computerScore == 1) {
			System.out.println("You win");
		}
		else{
			System.out.println("Computer wins");
		}
		return 0;

	}
	
}
